#/bin/bash

fusermount -u /mnt/crypt

sshfs -o allow_other,default_permissions,uid=$(id -u),gid=$(id -g),IdentityFile=/home/cworobetz/.ssh/id_rsa sysadmin@sync.worobetz.ca:/var/www/sync/crypt /mnt/crypt
