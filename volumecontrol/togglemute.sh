#!/bin/bash

# Toggle the mute for the currently active "sink" (device)

sink=$(pactl list short | grep RUNNING | sed -e 's,^\([0-9][0-9]*\)[^0-9].*,\1,' | head -n 1)

pactl set-sink-mute $sink toggle
