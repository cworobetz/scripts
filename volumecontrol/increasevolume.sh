#!/bin/bash

# Toggle the mute for the currently active "sink" (device)

vol=\+10%
sink=$(pactl list short | grep RUNNING | sed -e 's,^\([0-9][0-9]*\)[^0-9].*,\1,' | head -n 1)

pactl set-sink-mute $sink 0
pactl set-sink-volume $sink $vol
